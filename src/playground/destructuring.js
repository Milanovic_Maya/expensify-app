//object destructuring:
// const place = {
//     name: "Nara",
//     age: 1300,
//     location: {
//         country: "JP",
//         temp: 9
//     }
// };
// //setting default for name, and new key from temp:temperature
// const { name = "Osaka", age } = place;//same as name=place.name; age=place.age;
// const { country: countryName = "NP", temp: temperature } = place.location;

// // console.log(`${place.name} is ${place.age}.`);
// console.log(`${name} is ${age}.`);


// // console.log(`It's ${place.location.temp} in ${place.location.country}.`);
// console.log(`It's ${temperature} in ${countryName}.`);

//////////////////////////////////////////////////////////////////////////

//array destructuring:

const music = [
    "Hanggai",
    "Horse of Colors",
    "Samsara",
    "2016"
];
//destructure array:pull items from array:
const [band, album, song, year] = music;
//matches by position!!

// const [, album, song, year] = music; coma to skip element

//set default:[band, album, song, year=2018];



console.log(`You are listening to ${song} song from album ${album} (year: ${year}), by artist ${band}.`);
