import { createStore, combineReducers } from "redux";
import uuid from "uuid";


////////////////////////////////////////////////////
//actions:
//

const addExpense = ({ description = "", note = "", amount = 0, createdAt = 0 } = {}) => {
    return {
        type: "ADD_EXPENSE",
        expense: {
            id: uuid(),
            description,
            note,
            amount,
            createdAt
        }
    };
};

const removeExpense = ({ id } = {}) => {
    return {
        type: "REMOVE_EXPENSE",
        id
    };
};


const editExpense = (id, updates) => {
    return {
        type: "EDIT_EXPENSE",
        id,
        updates
    };
};


const setTextFilter = (text = "") => {
    return {
        type: "SET_TEXT_FILTER",
        text
    };
};


const sortByAmount = () => {
    return {
        type: "SORT_BY_AMOUNT",
    };
};


const sortByDate = () => {
    return {
        type: "SORT_BY_DATE",
    };
};


const setStartDate = (date) => {
    return {
        type: "SET_START_DATE",
        date
    };
};


const setEndDate = (date) => {
    return {
        type: "SET_END_DATE",
        date
    };
};

//reducers:
//make one for filters and other for expenses:
////////////////////////////////////////////////
//expenses reducer:

const expensesReducerDefaultState = [];

const expensesReducer = (state = expensesReducerDefaultState, action) => {
    switch (action.type) {
        case "ADD_EXPENSE":
            return [...state, action.expense];//spread operator or state.concat([item1,item2])
        case "REMOVE_EXPENSE":
            return state.filter(({ id }) => id !== action.id);
        case "EDIT_EXPENSE":
            return state.map(expense => {
                if (expense.id === action.id) {
                    return {
                        ...expense,
                        ...action.updates
                    };
                } else {
                    return expense;
                };
            });
        default:
            return state;
    };
};

//filters Reducer:

const filtersReducerDefaultState = {
    text: "",
    sortBy: "date",//amount or date
    startDate: undefined,
    endDate: undefined

}

const filtersReducer = (state = filtersReducerDefaultState, action) => {
    switch (action.type) {
        case "SET_TEXT_FILTER":
            return {
                ...state,
                text: action.text
            };
        case "SORT_BY_AMOUNT":
            return {
                ...state,
                sortBy: "amount"
            };
        case "SORT_BY_DATE":
            return {
                ...state,
                sortBy: "date"
            };
        case "SET_START_DATE":
            return {
                ...state,
                startDate: action.date
            };
        case "SET_END_DATE":
            return {
                ...state,
                endDate: action.date
            };
        default:
            return state;
    };
};

//create Store where all reducers' results will be assembled:
//for expenses part of state->expensesReducer will manage it;

const store = createStore(
    //combineReducers() takes object=>state (store) object
    combineReducers({
        expenses: expensesReducer,
        filters: filtersReducer
    })
);

//////////////////////////////////////////
//Get visible expenses
//
const getVisibleExpenses = (expenses, { text, sortBy, startDate, endDate }) => {
    return expenses.filter(expense => {
        const startDateMatch = typeof startDate !== "number" || expense.createdAt >= startDate;
        const endDateMatch = typeof endDate !== "number" || expense.createdAt <= endDate;
        const textMatch = expense.description.toLowerCase().includes(text.toLowerCase());

        return startDateMatch && endDateMatch && textMatch;
    }).sort((a, b) => {
        if (sortBy === "date") {
            return a.createdAt < b.createdAt ? 1 : -1;
        } else if (sortBy === "amount") {
            return a.amount < b.amount ? 1 : -1;
        };
    })
};


/////////////////////////////////////////////////
//dispatch to store=>create action:

store.subscribe(() => {
    const state = store.getState();
    const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);

    console.log(visibleExpenses);

    // console.log(store.getState());
});


// store.dispatch() returns action object back:
const expense1 = store.dispatch(addExpense({ description: "cat food", amount: 1600, createdAt: -21000 }));
const expense2 = store.dispatch(addExpense({ description: "cat toys", amount: 2000, createdAt: -1000 }));


// store.dispatch(removeExpense({ id: expense1.expense.id }));
// store.dispatch(editExpense(expense2.expense.id, { amount: 1000 }));
// store.dispatch(setTextFilter("toys"));
// store.dispatch(setTextFilter());
store.dispatch(sortByAmount());
// store.dispatch(sortByDate());

// store.dispatch(setStartDate(0)); 
// store.dispatch(setStartDate());

// store.dispatch(setEndDate(1250));


//////////////////////////////////////////////////////////////////////////////
//mock of global state object:

const demoState = {
    expenses: [
        {
            id: "lakipaki",
            description: "cat_food_felix",
            note: "feed_the_cats",
            amount: 1500,
            createdAt: 0
        }
    ],
    filters: {
        text: "food",
        sortBy: "amount",//amount or date
        startDate: undefined,
        endDate: undefined
    }
};


// const cat={
//     name:"Laki",
//     age:1
// };
// console.log({
//     ...cat,//enables overriding properties
//     color:"ginger",
//     name:"Paki",
// });

