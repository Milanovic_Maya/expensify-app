import React from "react";
import ReactDOM from "react-dom";

const Info = (props) => {
    return (
        <div>
            <h1>Info</h1>
            <p>The info is : {props.info}</p>
        </div>
    );
};


const withAdminWarning = (WrappedComponent) => {

    // returns new component:instance of argument component and some plus!
    return (props) => (
        <div>
            {props.isAdminProp && <p>Admin message</p>}
            <WrappedComponent {...props} />
        </div>
    );
};

const requireAuthentication = (WrappedComponent) => {
    //here returns stateless component, but can also class component;
    return (props) => (
        <div>
            {props.isAuthenticated ? (
                <WrappedComponent {...props} />
            ) : (
                    <p>You must have permission!</p>
                )}
        </div>
    );
};

const AdminInfo = withAdminWarning(Info);
const AuthInfo = requireAuthentication(Info);

// ReactDOM.render(<AdminInfo isAdminProp={true} info={"shh...cats are playing"} />, document.getElementById("root"));
ReactDOM.render(<AuthInfo isAuthenticated={true} info={"shh...cats are playing"} />, document.getElementById("root"));