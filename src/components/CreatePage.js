import React from "react";
import ExpenseForm from "./ExpenseForm";
import { connect } from "react-redux";
import { startAddExpense } from "../actions/expenses";

export class CreatePage extends React.Component {
    onSubmit = (expense) => {
        // props.dispatch(startAddExpense(expense));//dispatch action creator on user event
        this.props.startAddExpense(expense);
        this.props.history.push("/");//redirect to home page
    };

    render() {
        return (
            <div>
                <p>CreatePage</p>
                <ExpenseForm onSubmit={this.onSubmit} />
            </div>
        );
    };
};

const mapDispatchToProps = dispatch => ({ startAddExpense: expense => dispatch(startAddExpense(expense)) });

export default connect(undefined, mapDispatchToProps)(CreatePage);
