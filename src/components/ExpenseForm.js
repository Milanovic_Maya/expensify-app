import React from "react";
import moment from "moment";
import { SingleDatePicker } from "react-dates";


const now = moment();//current point in time;
console.log(now.format("Do MMM, YYYY."));


class ExpenseForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            description: props.expense ? props.expense.description : "",
            note: props.expense ? props.expense.note : "",
            amount: props.expense ? (props.expense.amount / 100).toString() : "",
            createdAt: props.expense ? moment(props.expense.createdAt) : moment(),
            calendarFocused: false,
            error: ""
        };
    };

    onDescriptionChange = e => {
        const description = e.target.value;
        this.setState(() => ({ description }));
    };

    onNoteChange = e => {
        const note = e.target.value;//if not defined here, must use:
        //e.persist();
        this.setState(() => ({ note }));
    };

    onAmountChange = e => {
        const amount = e.target.value;
        //condition to render only numbers with 2 decimals:
        //regex101.com see explanation!
        if (!amount || amount.match(/^\d{1,}(\.\d{0,2})?$/)) {
            this.setState(() => ({ amount }));
        };
    };

    onDateChange = createdAt => {
        if (createdAt) {
            this.setState(() => ({ createdAt }));
        };
    };

    onFocusChange = ({ focused }) => {
        this.setState(() => ({ calendarFocused: focused }));
    };

    onFormSubmit = e => {
        e.preventDefault();
        if (!this.state.description || !this.state.amount) {
            //set error state and message
            this.setState(() => ({ error: "Please provide descrition and amount." }));
        } else {
            //clear error and log "submitted"
            this.setState(() => ({ error: "" }));
            this.props.onSubmit({
                description: this.state.description,
                amount: parseFloat(this.state.amount, 10) * 100,
                createdAt: this.state.createdAt.valueOf(),
                note: this.state.note
            })
            console.log("submitted");
        };
    };

    render() {
        return (
            <div>
                {this.state.error && <p>{this.state.error}</p>}
                <form onSubmit={this.onFormSubmit}>
                    <input
                        type="text"
                        placeholder="Description"
                        autoFocus
                        value={this.state.description}
                        onChange={this.onDescriptionChange}
                    />
                    <input
                        type="number"
                        placeholder="Amount"
                        value={this.state.amount}
                        //restrict input:
                        onChange={this.onAmountChange}

                    />
                    <SingleDatePicker
                        date={this.state.createdAt}
                        onDateChange={this.onDateChange}//function called with moment
                        focused={this.state.calendarFocused}
                        onFocusChange={this.onFocusChange}
                        numberOfMonths={1}
                        isOutsideRange={() => { false }}//access days in past too
                    />
                    <textarea
                        placeholder="Notes..."
                        cols="30"
                        rows="10"
                        value={this.state.note}
                        onChange={this.onNoteChange}
                    />
                    <button >
                        Add expense
                    </button>
                </form>
            </div>
        );
    };
};

export default ExpenseForm;