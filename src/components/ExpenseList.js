import React from "react";
import { connect } from "react-redux";//connects component to redux store
import ExpenseItem from "./ExpenseItem";
import selectExpenses from "../selectors/expenses_selector";

export const ExpenseList = (props) => {
    return (
        <div>
            {
                props.expenseList.length === 0 && <p>No expenses</p>
            }
            Expense List:
            <ul>
                {
                    props.expenseList.map(expense => {
                        return (
                            <li key={expense.id}>
                                <ExpenseItem {...expense} />
                            </li>
                        )
                    })
                }
            </ul>
        </div>
    );
};

const mapStateToProps = state => {
    return {
        expenseList: selectExpenses(state.expenses, state.filters)
    };
};

export default connect(mapStateToProps)(ExpenseList);;