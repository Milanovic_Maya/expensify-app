import React from "react";
import { Link } from "react-router-dom";
import moment from "moment";
import numeral from "numeral";

const ExpenseItem = ({ description, amount, createdAt, id }) => {
    return (
        <div>
            <Link to={`/edit/${id}`}> <h3>{description}</h3> </Link>
            <p>amount: {numeral(amount/10000).format("$0.0.00")}</p>
            <p>created at: {moment(createdAt).format("MMMM Do, YYYY")}</p>
        </div>
    );
};
export default ExpenseItem;