const moment = require.requireActual("moment");

export default (timestamp = 0) => {
    //real app will use moment library, and test this one
    return moment(timestamp);
};
