//test unconnected version of component:
import React from "react";
import { shallow } from "enzyme";
import { ExpenseList } from "../../components/ExpenseList";
import expenses from "../fixtures/expenses";

test("should render expenseList with expenses", () => {
    const wrapper = shallow(<ExpenseList expenseList={expenses} />);
    expect(wrapper).toMatchSnapshot();
});
test("should render expenseList with empty message", () => {
    const wrapper = shallow(<ExpenseList expenseList={[]} />);
    expect(wrapper).toMatchSnapshot();
});