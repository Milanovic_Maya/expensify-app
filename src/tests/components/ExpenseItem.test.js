import React from "react";
import { shallow } from "enzyme";//renderer from enzyme
import ExpenseItem from "../../components/ExpenseItem";
import expenses from "../fixtures/expenses";

test("should render ExpenseItem with expense props", () => {
    const wrapper = shallow(<ExpenseItem {...expenses[1]} />);
    expect(wrapper).toMatchSnapshot();
});