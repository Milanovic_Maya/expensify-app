import React from "react";
import { CreatePage } from "../../components/CreatePage";
import { shallow } from "enzyme";
import expenses from "../fixtures/expenses";

//declare variables:
let startAddExpense, history, wrapper;


//to run some code before each test case, use jest method!!!
beforeEach(() => {
    //callback function that runs before each test case
    //spies for component's props-->
    startAddExpense = jest.fn();
    history = { push: jest.fn() };

    //we pass spies as test data to component testing-->
    wrapper = shallow(<CreatePage startAddExpense={startAddExpense} history={history} />);
});



test("should render  create expense page correctly", () => {
    //just make a snapshot-->
    expect(wrapper).toMatchSnapshot();
});



test("should handle on submit", () => {
    expect(wrapper).toMatchSnapshot();
    wrapper.find("ExpenseForm").prop("onSubmit")(expenses[1]);

    //expect our spies to be called with correct test data-->
    expect(history.push).toHaveBeenLastCalledWith("/");
    expect(startAddExpense).toHaveBeenLastCalledWith(expenses[1]);
});