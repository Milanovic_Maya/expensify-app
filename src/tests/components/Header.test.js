// import ReactShallowRenderer from "react-test-renderer/shallow";
//use ReactShallowRenderer to render Header; without lifecycle events, children or user interactions;
import React from "react";
import Header from "../../components/Header";
import {
    shallow
} from "enzyme";


test("should render header correctly", () => {
    // //new instance of renderer;
    // const renderer = new ReactShallowRenderer();

    // //render what we want :
    // renderer.render(<Header />);

    // expect(renderer.getRenderOutput()).toMatchSnapshot();

    // console.log(renderer.getRenderOutput());

    /////////////////////////////////////////
    //shallow render Header:
    const wrapper = shallow( < Header / > );
    expect(wrapper).toMatchSnapshot();
});