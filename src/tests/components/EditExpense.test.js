import React from "react";
import { EditPage } from "../../components/EditPage";
import { shallow } from "enzyme";
import expenses from "../fixtures/expenses";

//setup spies etc;
let startEditExpense, history, wrapper, startRemoveExpenses;

//beforeEach(()=>{})
beforeEach(() => {
    startEditExpense = jest.fn();
    startRemoveExpenses = jest.fn();
    history = { push: jest.fn() };
    wrapper = shallow(<EditPage
        startEditExpense={startEditExpense}
        startRemoveExpenses={startRemoveExpenses}
        history={history}
        expense={expenses[2]}
    />);
});

test("should render edit expense page correctly", () => {
    expect(wrapper).toMatchSnapshot();
});


//test on edit
test("should edit expense on submit", () => {
    //find expense form and call it to see does fn gets called:
    wrapper.find("ExpenseForm").prop("onSubmit")(expenses[2]);
    //what should've happened to those spies:
    expect(history.push).toHaveBeenLastCalledWith("/");
    expect(startEditExpense).toHaveBeenLastCalledWith(expenses[2].id, expenses[2]);
});

//call spy startEditExpense with correct data expense={expenses[0]}
//call history to be called with "/"

//test on remove

test("should remove expense on submit", () => {
    //find expense form and call it to see does fn gets called:
    wrapper.find("button").simulate("click");
    //what should've happened to those spies:
    expect(history.push).toHaveBeenLastCalledWith("/");
    expect(startRemoveExpenses).toHaveBeenLastCalledWith({ id: expenses[2].id });
});

test('should handle startEditExpense', () => {
  wrapper.find('ExpenseForm').prop('onSubmit')(expenses[2]);
  expect(history.push).toHaveBeenLastCalledWith('/');
  expect(startEditExpense).toHaveBeenLastCalledWith(expenses[2].id, expenses[2]);
});
