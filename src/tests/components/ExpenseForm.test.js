import React from "react";
import { shallow } from "enzyme";
import ExpenseForm from "../../components/ExpenseForm";
import expenses from "../fixtures/expenses";
import moment from "moment";

test("should render expense form", () => {
    const wrapper = shallow(<ExpenseForm />);
    expect(wrapper).toMatchSnapshot();//second snapshot will fail because of moment() date, data is changing
});

//"should render expense form with expense data"

test("should correctly render expense form with expense data", () => {
    const wrapper = shallow(<ExpenseForm expense={expenses[0]} />);
    expect(wrapper).toMatchSnapshot();//second snapshot will fail because of moment() date, data is changing
});

//form submission:

test("should render error for invalid form submission", () => {
    const wrapper = shallow(<ExpenseForm />);
    //take snapshot before we make changes
    expect(wrapper).toMatchSnapshot();

    wrapper.find("form").simulate("submit", { preventDefault: () => { } });//access to form element
    //simulate event, second argument simulates preventDefault, because we get undefined.preventDefault()

    //fetch state of form component to see if error is !== to ""
    expect(wrapper.state("error").length).toBeGreaterThan(0);
    expect(wrapper).toMatchSnapshot();
});


test("should set description on input change", () => {
    const wrapper = shallow(<ExpenseForm />);

    const value = "new value";

    //1.access the element:
    //2.simulate the change event:
    //--at(takes index of element in component, first input, second input ...)
    wrapper.find("input").at(0).simulate("change", { target: { value } });

    expect(wrapper.state("description")).toBe(value);
});

//should set note on textarea change:

test("should set note on textarea change", () => {
    const wrapper = shallow(<ExpenseForm />);

    const value = "laki&paki";

    wrapper.find("textarea").simulate("change", { target: { value } });
    expect(wrapper.state("note")).toBe(value);
});

//should set amount if valid input
//"23.50"==="23.50"
test("should set amount if valid input", () => {
    const wrapper = shallow(<ExpenseForm />);
    const value = "23.50";
    wrapper.find("input").at(1).simulate("change", { target: { value } });
    expect(wrapper.state("amount")).toBe(value);
});

//should not set amount if invalid input
//"12.122" !== ""
test("should not set amount if invalid input", () => {
    const wrapper = shallow(<ExpenseForm />);
    const value = "12.122";
    wrapper.find("input").at(1).simulate("change", { target: { value } });
    expect(wrapper.state("amount")).toBe("");
});

/////////////////////////////
///spies:

test("should call onSubmit for valid form submission", () => {
    const onSubmitSpy = jest.fn();//setting spy

    // onSubmitSpy("Laki", "Paki");//calling the spy

    //check if the spy function is called:
    // expect(onSubmitSpy).toHaveBeenCalled();

    // expect(onSubmitSpy).toHaveBeenCalledWith("Laki", "Paki");


    //component rendered with spy:
    const wrapper = shallow(<ExpenseForm expense={expenses[0]} onSubmit={onSubmitSpy} />);
    //submit form:
    wrapper.find("form").simulate("submit", { preventDefault: () => { } });//access to form element
    expect(wrapper.state('error')).toBe("");

    //check if spy is called with correct stuff:
    // expect(onSubmitSpy).toHaveBeenLastCalledWith(expenses[0]);//fail, there's no id;

    expect(onSubmitSpy).toHaveBeenLastCalledWith({
        description: expenses[0].description,
        amount: expenses[0].amount,
        note: expenses[0].note,
        createdAt: expenses[0].createdAt
    });
});
///////////////////////////////////////
//access props on children

test("should set new date on date change", () => {
    const now = moment();
    const wrapper = shallow(<ExpenseForm />);
    wrapper.find("withStyles(SingleDatePicker)").prop('onDateChange')(now);
    expect(wrapper.state('createdAt')).toEqual(now);
});

test('should set calendar focus on change', () => {
    const focused = true;
    const wrapper = shallow(<ExpenseForm />);

    wrapper.find("withStyles(SingleDatePicker)").prop('onFocusChange')({ focused });
    expect(wrapper.state('calendarFocused')).toBe(focused);
});
