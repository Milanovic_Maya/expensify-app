import filtersReducer from "../../reducers/filters_reducer";
import moment from "moment";

// test cases:
//1.does default values set correctly when redux store initates?
//redux dispatches special action for that {type:"@@INIT"};

test("should setup default values", () => {
    const state = filtersReducer(undefined, {
        type: "@@INIT"
    });
    expect(state).toEqual({
        text: "",
        sortBy: "date",
        startDate: moment().startOf("month"),
        endDate: moment().endOf("month")
    });
});

//2.when we dispatch action, sortBy changes to "amount"

test("should set sortBy to amount", () => {
    const state = filtersReducer(undefined, { type: "SORT_BY_AMOUNT" });
    expect(state.sortBy).toBe("amount");
});


//3.when we dispatch action, sortBy changes to "date"

test("should set sortBy to date", () => {
    //default state, current state value
    //test data
    const currentState = {
        text: "",
        startDate: undefined,
        endDate: undefined,
        sortBy: "amount"
    };
    const action = { type: "SORT_BY_DATE" };

    const state = filtersReducer(currentState, action);

    //assertion of what comes back:
    expect(state.sortBy).toBe("date");
});


//4.should set text filter:

test("should set text filter", () => {
    const text = "cats";
    const action = { type: "SET_TEXT_FILTER", text };
    const state = filtersReducer(undefined, action);

    expect(state.text).toBe(text);
});

//5.should set start Date filter;
test('should set startDate filter', () => {
    const startDate = moment();
    const action = {
        type: 'SET_START_DATE',
        date: startDate
    };
    const state = filtersReducer(undefined, action);
    expect(state.startDate).toEqual(startDate);
});

//6.should set end Date filter;

test('should set endDate filter', () => {
    const endDate = moment();
    const action = {
        type: 'SET_END_DATE',
        date: endDate
    };
    const state = filtersReducer(undefined, action);
    expect(state.endDate).toEqual(endDate);
});
