import expensesReducer from "../../reducers/expenses_reducer";
import expenses from '../fixtures/expenses';


test("should set default state", () => {
    const state = expensesReducer(undefined, { type: "@@INIT" });
    expect(state).toEqual([]);
});


// remove expense by id:
test("should remove expense by id", () => {
    const action = { type: "REMOVE_EXPENSE", id: expenses[1].id };
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([expenses[0], expenses[2]]);
});

//not remove expense if id not found:
test("should not remove expense if id not found", () => {
    const action = { type: "REMOVE_EXPENSE", id: "-1" };
    const state = expensesReducer(expenses, action);
    expect(state).toEqual(expenses);
});

//should add an expense (array has another value);
test("should add an expense", () => {
    const expense = {
        id: '109',
        description: 'music cd',
        note: '',
        createdAt: 20000,
        amount: 2950
    };
    const action = { type: "ADD_EXPENSE", expense };
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([...expenses, expense]);
});

//should edit expense
test("should edit expense", () => {
    const amount = 13300;
    const action = { type: "EDIT_EXPENSE", id: expenses[1].id, updates: { amount } };
    const state = expensesReducer(expenses, action);
    expect(state[1].amount).toBe(amount);
});

//should not edit expense if id of expense not found
test('should not edit an expense if id not found', () => {
    const amount = 12770;
    const action = {
        type: 'EDIT_EXPENSE',
        id: '-12',
        updates: {
            amount
        }
    };
    const state = expensesReducer(expenses, action);
    expect(state).toEqual(expenses);
});


test("should set expenses", () => {
    const action = {
        type: "SET_EXPENSES",
        expenses: [expenses[1]]
    };
    const state = expensesReducer(expenses, action);
    expect(state).toEqual([expenses[1]]);
}); 