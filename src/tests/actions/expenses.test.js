import { addExpense, setExpenses, editExpense, removeExpense, startAddExpense, startSetExpenses,startEditExpense, startRemoveExpenses } from "../../actions/expenses";
import expenses from "../fixtures/expenses";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import database from "../../firebase/firebase";

const createMockStore = configureMockStore([thunk]);

beforeEach((done) => {
    const expensesData = {};
    expenses.forEach(({ id, description, note, amount, createdAt }) => {
        expensesData[id] = { description, note, amount, createdAt };
    });
    database.ref("expenses").set(expensesData).then(() => done());
});



//call these action generators within test cases;

//remove expenses
test("should setup remove expense action obj", () => {
    const action = removeExpense({
        id: "123abc"
    });
    expect(action).toEqual({
        type: "REMOVE_EXPENSE",
        id: "123abc"
    });
});

//REMOVE FROM FIREBASE________________________________

test('should remove expense from firebase', (done) => {
    const store = createMockStore({});
    const id = expenses[2].id;
    store.dispatch(startRemoveExpenses({ id })).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: 'REMOVE_EXPENSE',
            id
        });
        return database.ref(`expenses/${id}`).once('value');
    }).then((snapshot) => {
        expect(snapshot.val()).toBeFalsy();
        done();
    });
});



//edit expenses
test("should setup edit expense action obj", () => {
    const action = editExpense("123abc", {
        note: "cats"
    });
    expect(action).toEqual({
        type: "EDIT_EXPENSE",
        id: "123abc",
        updates: {
            note: "cats"
        }
    });
});


test('should edit expense from firebase', (done) => {
    const store = createMockStore({});
    const id = expenses[0].id;
    const updates = { amount: 21045 };
    store.dispatch(startEditExpense(id, updates)).then(() => {
      const actions = store.getActions();
      expect(actions[0]).toEqual({
        type: 'EDIT_EXPENSE',
        id,
        updates
      });
      return database.ref(`expenses/${id}`).once('value');
    }).then((snapshot) => {
      expect(snapshot.val().amount).toBe(updates.amount);
      done();
    });
  });

//add expenses:

test("should setup add expense action object with provided values", () => {
    const action = addExpense(expenses[2]);
    //we don't know what data will come back; id is changing, it's unique;

    //we know it will be equal to object:
    expect(action).toEqual({
        type: "ADD_EXPENSE",
        expense: expenses[2]
    });
});

// __________ASYNC ACTIONS_____________________________________________________________________

//test for async generator

test("should add expense to database and store", (done) => {
    //is database updated and is action dispatched;
    const store = createMockStore({});
    const expenseData = { description: "cat food", amount: 2300, note: "carnilove", createdAt: 12345677 };

    // 1. call async action:
    store.dispatch(startAddExpense(expenseData))
        .then(() => {
            const actions = store.getActions();//returns array of actions

            //only if the first action is dispatched(startAddExpenses)
            expect(actions[0]).toEqual({
                type: "ADD_EXPENSE",
                expense: { id: expect.any(String), ...expenseData }
            });

            // 2. is firebase populated with data?
            return database.ref(`expenses/${actions[0].expense.id}`).once("value");
        }).then((snapshot) => {
            expect(snapshot.val()).toEqual(expenseData)
            done();
            //JEST waits async, knows we're done making assertions
        });
});

test('should add expense with defaults to database and store', (done) => {
    const store = createMockStore({});
    const expenseDefaults = {
        description: '',
        amount: 0,
        note: '',
        createdAt: 0
    };

    //pass empty object as default_______->
    store.dispatch(startAddExpense({})).then(() => {
        const actions = store.getActions();
        expect(actions[0]).toEqual({
            type: 'ADD_EXPENSE',
            expense: {
                id: expect.any(String),
                ...expenseDefaults
            }
        });

        return database.ref(`expenses/${actions[0].expense.id}`).once('value');
    }).then((snapshot) => {
        expect(snapshot.val()).toEqual(expenseDefaults);
        done();
    });
});



test("should setup set expense action object with data", () => {
    const action = setExpenses(expenses);
    expect(action).toEqual({ type: "SET_EXPENSES", expenses });
});

test("should fetch the expenses from firebase", (done) => {
    const store = createMockStore({});
    store.dispatch(startSetExpenses())
        .then(() => {
            const actions = store.getActions();
            expect(actions[0]).toEqual({
                type: "SET_EXPENSES",
                expenses
            });
            done();
        });
});