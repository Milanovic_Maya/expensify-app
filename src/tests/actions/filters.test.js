import {
    setStartDate,
    setEndDate,
    sortByAmount,
    sortByDate,
    setTextFilter
} from "../../actions/filters";

import moment from "moment";

//start date
test("should generate action obj startDate", () => {
    const action = setStartDate(moment(0));
    //then, make assertion:
    expect(action).toEqual({
        type: "SET_START_DATE",
        date: moment(0)
    });
});
//end date
test("should generate action obj endDate", () => {
    const action = setEndDate(moment(0));
    //then, make assertion:
    expect(action).toEqual({
        type: "SET_END_DATE",
        date: moment(0)
    });
});

//sort by date

test("action obj sort date", () => {
    const action = sortByDate();
    expect(action).toEqual({
        type: "SORT_BY_DATE",
    });
})

//sort by amount

test("action obj sort amount", () => {
    const action = sortByAmount();
    expect(action).toEqual({
        type: "SORT_BY_AMOUNT",
    });
});

//text filter

test("action obj text filter with text", () => {
    const action = setTextFilter("food");
    expect(action).toEqual({
        type: "SET_TEXT_FILTER",
        text: "food"
    });
});

test("action obj text filter with default", () => {
    const action = setTextFilter();
    expect(action).toEqual({
        type: "SET_TEXT_FILTER",
        text: ""
    });
});