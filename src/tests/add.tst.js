const add = (a, b) => a + b;
const generateGreeting = (name = `Anonymous`) => `Hello, ${name}!`;

test("should add two nums", () => {
    const result = add(3, 4);
    // if (result !== 7) {
    //     throw new Error(`${result} is not correct`);
    expect(result).toBe(7);
    // };
});

test("should say hello, laki & paki!", () => {
    const greet = generateGreeting("Laki & Paki");
    expect(greet).toBe(`Hello, Laki & Paki!`);
});

test("no name greeting", () => {
    const result = generateGreeting();
    expect(result).toBe(`Hello, Anonymous!`);
});