import React from "react";
import { BrowserRouter, Route, Switch, Link, NavLink } from "react-router-dom";
import "normalize.css/normalize.css";
import "../styles/styles.scss";
import Header from "../components/Header";
import DashboardPage from "../components/DashboardPage";
import NotFoundPage from "../components/NotFoundPage.js";
import HelpPage from "../components/HelpPage";
import CreatePage from "../components/CreatePage";
import EditPage from "../components/EditPage";
import LoginPage from "../components/LoginPage";


const AppRouter = () => {
    return (
        <BrowserRouter>
            <div>
                <Header />
                <Switch>
                    <Route exact path="/dashboard" component={DashboardPage} />
                    <Route path="/create" component={CreatePage} />
                    <Route path="/edit/:id" component={EditPage} />
                    <Route path="/help" component={HelpPage} />
                    <Route path="/" component={LoginPage} />
                    <Route component={NotFoundPage} />
                </Switch>
            </div>
        </BrowserRouter>
    )
}

//browser throws error because there's no create page on server
//need to change dev server to return html for all routes
//go to webpack config file=> devServer:{...historyApiFallback: true}
//then reset dev-server in terminal

//Swicth renders only first component from Route, where path matches url
export default AppRouter;