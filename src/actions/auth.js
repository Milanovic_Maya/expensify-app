//firebase related stuff
import { firebase, googleAuthProvider } from "../firebase/firebase";

// function to dispatch action, to start login:
export const startLogin = () => {
    return () => {
        return firebase.auth().signInWithPopup(googleAuthProvider)
    };
};

