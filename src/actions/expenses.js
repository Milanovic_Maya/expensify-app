import uuid from "uuid";
import database from "../firebase/firebase";

//firebase use push
//dispatch action
//redirect

//actions:
//


//async redux:____________________________________________________
// call action generator in component
//action generator returns a function
//component will dispatch that function--need middleware for that!
//redux will execute that function (supported with middleware)
//there will be firebase code
//then dispatch another normal action (object)
//the rest

// action object for changing the store
export const addExpense = expense => ({
    type: "ADD_EXPENSE",
    expense
});

export const startAddExpense = (expenseData = {}) => {
    //expenseData from user form;
    return dispatch => {
        //expenseData is object, here is destructured:
        const {
            description = '',
                note = '',
                amount = 0,
                createdAt = 0
        } = expenseData;

        //that data is going to be sent to firebase:
        const expense = {
            description,
            note,
            amount,
            createdAt
        };

        //we use that data and push to firebase-->
        //when async action completes, promise is returned...goes to then()
        //in then we dispatch action to create new expense
        //but this doesn't read from firebase!!!!!! for that, another func is needed


        //when we return this...we can chain another then on it
        return database.ref("expenses").push(expense)
            .then(ref => dispatch(addExpense({
                id: ref.key,
                ...expense
            })))
        // .catch()
    };
};


export const removeExpense = ({
    id
} = {}) => {
    return {
        type: "REMOVE_EXPENSE",
        id
    };
};



//_________________________EDTT:

export const editExpense = (id, updates) => {
    return {
        type: "EDIT_EXPENSE",
        id,
        updates
    };
};

export const startEditExpense = (id, updates) => {
    return (dispatch) => {
        return database.ref(`expenses/${id}`).update(updates).then(() => {
            dispatch(editExpense(id, updates));
        });
    };
};


//action to change store-to populate store with value from firebase

export const setExpenses = (expenses) => {
    return {
        type: "SET_EXPENSES",
        expenses
    };
};

//async call to database happens here:
export const startSetExpenses = () => {
    // ..return func that has access to dispatch:
    return dispatch => {
        //fetch expenses once:
        return database.ref("expenses").once("value")
            .then(snapshot => {
                //snapshot is parsed to array
                const expenseList = [];

                snapshot.forEach(childSnapshot => {
                    expenseList.push({
                        id: childSnapshot.key,
                        ...childSnapshot.val()
                    });
                })
                //dispatch them (expenseList) to store with setExpenses
                dispatch(setExpenses(expenseList));
            })
    };
};

export const startRemoveExpenses = ({
    id
} = {}) => {
    return (dispatch) => {
        return database.ref(`expenses/${id}`).remove().then(() => {
            dispatch(removeExpense({
                id
            }));
        });
    };
};