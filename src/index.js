import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import AppRouter from "./routers/AppRouter";
import { startSetExpenses } from "./actions/expenses";
// import { setTextFilter } from "./actions/filters";
import getVisibleExpenses from "./selectors/expenses_selector";
import 'react-dates/lib/css/_datepicker.css';
import "./firebase/firebase";
import { firebase } from "./firebase/firebase";

const store = configureStore();

// store.dispatch(addExpense({ description: "cat lotion", amount: "1200", createdAt: 1000 }));
// store.dispatch(addExpense({ description: "cat treats and food", amount: "800", createdAt: 2000 }));
// store.dispatch(addExpense({ description: "cat food", amount: "3800", createdAt: 2000 }));
// store.dispatch(addExpense({ description: "cat toys", amount: "900", createdAt: 2000 }));


// store.dispatch(setTextFilter("lotion"));

// setTimeout(()=>{
//     store.dispatch(setTextFilter("food"));
// },3000);


const state = store.getState();
const visibleExpenses = getVisibleExpenses(state.expenses, state.filters);

// console.log(visibleExpenses);


const jsx = (
    <Provider store={store}>
        <AppRouter />
    </Provider>
);



ReactDOM.render(<p>Loading...</p>, document.getElementById("root"));

store.dispatch(startSetExpenses()).then(() => {
    ReactDOM.render(jsx, document.getElementById("root"));
});


firebase.auth().onAuthStateChanged((user) => {
    if (user) {
        console.log("log in");
    } else {
        console.log("log out");
    }
});