//1.connect to the database
import * as firebase from "firebase";

//2.grab config obj from firebase from browser:
const config = {
    apiKey: "AIzaSyBkQku7p3wNHr-jl-ACBbMW9GhYEm6jBAs",
    authDomain: "expensify-86486.firebaseapp.com",
    databaseURL: "https://expensify-86486.firebaseio.com",
    projectId: "expensify-86486",
    storageBucket: "expensify-86486.appspot.com",
    messagingSenderId: "343308510639"
};

// 3.initialize Firebase
firebase.initializeApp(config);

// 4.access database:
const database = firebase.database();

// 5.setup provider:
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();


export {
    firebase,
    googleAuthProvider,
    database as
    default
};